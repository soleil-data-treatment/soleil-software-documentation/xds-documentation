# Aide et ressources de XDS pour Synchrotron SOLEIL

[<img src="https://strucbio.biologie.uni-konstanz.de/xdswiki/resources/assets/frame.png?2f0cb" width="150"/>](https://strucbio.biologie.uni-konstanz.de/xdswiki/index.php/Main_Page)

## Résumé

- estimation bruit de fond. 
chercher des taches de Bragg.
indexation, proposer des mailles.
integration, affinement -> taches intégrées.
verification de la maille, chercher une plus autre symétrie
- Privé

## Sources

- Code source: https://xds.mr.mpg.de/html_doc/XDS.html
- Documentation officielle: https://strucbio.biologie.uni-konstanz.de/xdswiki/index.php/Main_Page

## Navigation rapide

| Tutoriaux | Page pan-data |
| - | - |
| [Tutoriel d'installation officiel](https://xds.mr.mpg.de/html_doc/downloading.html) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/130/xds) |
| [Tutoriaux officiels](https://strucbio.biologie.uni-konstanz.de/xdswiki/index.php/Topics) |   |

## Installation

- Systèmes d'exploitation supportés: Linux
- Installation: Facile (tout se passe bien),  pas beaucoup de dépendances,  standalone

## Format de données

- en entrée: hdf5,  cbf,  text,  img
- en sortie: text,  listes de hkl
- sur un disque dur
